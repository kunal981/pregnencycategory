package com.jack.pregnancycategory.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jack.pregnancycategory.R;
import com.jack.pregnancycategory.act.AboutUsActivity;

public class Tabbar extends FrameLayout implements OnClickListener {

	private Context context;
	private OnClickListener clickListener;

	Button buttonAboutUs, buttonOtc, buttonHerbals, buttonRecentsSearch;

	public Tabbar(Context context) {
		super(context);
		this.context = context;
		if (!isInEditMode())
			init();

	}

	public Tabbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		if (!isInEditMode())
			init();
	}

	private void init() {
		LayoutInflater layoutInflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.layout_footer, this);
		buttonAboutUs = (Button) findViewById(R.id.button_about_us);
		buttonOtc = (Button) findViewById(R.id.button_OTC);
		buttonHerbals = (Button) findViewById(R.id.button_herbals);
		buttonRecentsSearch = (Button) findViewById(R.id.button_recents_search);
		buttonAboutUs.setOnClickListener(this);
		buttonOtc.setOnClickListener(this);
		buttonHerbals.setOnClickListener(this);
		buttonRecentsSearch.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (clickListener != null)
			clickListener.onClick(v);

		switch (v.getId()) {
		case R.id.button_about_us:
			Intent intent = new Intent(context, AboutUsActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			((Activity) context).startActivity(intent);
			((Activity) context).overridePendingTransition(R.anim.fade_in,
					R.anim.fade_out);
			break;
		case R.id.button_OTC:
			Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();

			break;
		case R.id.button_herbals:
			Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();

			break;
		case R.id.button_recents_search:
			Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();

			break;

		default:
			break;
		}

	}

	@Override
	public void setOnClickListener(OnClickListener l) {
		clickListener = l;
	}

}
