package com.jack.pregnancycategory.act;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.jack.pregnancycategory.R;

public class AboutUsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
	}

}
