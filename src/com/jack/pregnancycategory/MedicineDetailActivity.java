package com.jack.pregnancycategory;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MedicineDetailActivity extends ActionBarActivity {

	private TextView titleText;
	private String title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_medicine_detail);
		titleText = (TextView) findViewById(R.id.button_title);
		title = getIntent().getExtras().getString("title");
		titleText.setText(title);
	}

}
