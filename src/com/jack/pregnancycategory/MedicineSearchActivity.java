package com.jack.pregnancycategory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MedicineSearchActivity extends ActionBarActivity {

	private Button buttonMedicine1;
	private Button buttonMedicine2;
	private Button buttonMedicine3;
	private Button buttonMedicine4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_medicine_search_result);

		buttonMedicine1 = (Button) findViewById(R.id.button_medicine_1);
		buttonMedicine2 = (Button) findViewById(R.id.button_medicine_2);
		buttonMedicine3 = (Button) findViewById(R.id.button_medicine_3);
		buttonMedicine4 = (Button) findViewById(R.id.button_medicine_4);
		buttonMedicine1.setOnClickListener(new MedicineClickListener());
		buttonMedicine2.setOnClickListener(new MedicineClickListener());
		buttonMedicine3.setOnClickListener(new MedicineClickListener());
		buttonMedicine4.setOnClickListener(new MedicineClickListener());
	}

	class MedicineClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			String title = ((Button) findViewById(v.getId())).getText()
					.toString();
			Intent intent = new Intent(MedicineSearchActivity.this,
					MedicineDetailActivity.class);

			intent.putExtra("title", title);
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

		}
	}

}
